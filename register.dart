import 'package:flutter/material.dart';
import 'package:myapp/dashboard.dart';
import 'package:myapp/login.dart';

class Register extends StatelessWidget {
  const Register({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register Account"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
                obscureText: false,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Create Username',
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Create Password',
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Retype Password',
                )),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () => {
                Navigator.push(
                    context, MaterialPageRoute(builder: (context) => LogIn()))
              },
              child: const Text("Register"),
            ),
          ),
        ],
      ),
    );
  }
}
