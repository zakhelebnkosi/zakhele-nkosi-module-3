import 'package:flutter/material.dart';
import 'package:myapp/featurescreen1.dart';
import 'package:myapp/featurescreen2.dart';
import 'package:myapp/home.dart';
import 'package:myapp/logIn.dart';
import 'package:myapp/profile.dart';

class DashBoard extends StatelessWidget {
  const DashBoard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Dashboard"),
        ),
        body: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FeatureScreen1()))
                  },
                  child: const Text("Feature Screen 1"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FeatureScreen2()))
                  },
                  child: const Text("Feature Screen 2"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () => {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => UserProfile()))
                  },
                  child: const Text("Edit User Profile"),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: ElevatedButton(
                  onPressed: () => {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context) => HomePage()))
                  },
                  child: const Text("Logout"),
                ),
              ),
            ],
          ),
        ));
  }
}
