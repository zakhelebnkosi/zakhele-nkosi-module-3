import 'package:flutter/material.dart';
import 'package:myapp/dashboard.dart';

class UserProfile extends StatelessWidget {
  const UserProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Edit User Profile"),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Name',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Surname',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Email',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                  obscureText: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Phune Number',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: const TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Retype Password',
                  )),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: ElevatedButton(
                onPressed: () => {
                  Navigator.pop(context,
                      MaterialPageRoute(builder: (context) => const DashBoard()))
                },
                child: const Text("Save Changes"),
              ),
            ),
          ],
        ));
  }
}
