import 'package:flutter/material.dart';
import 'package:myapp/dashboard.dart';

class FeatureScreen1 extends StatelessWidget {
  const FeatureScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Feature Screen One"),
        ),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: ElevatedButton(
            onPressed: () => {
              Navigator.pop(
                  context, MaterialPageRoute(builder: (context) => DashBoard()))
            },
            child: const Text("Back"),
          ),
        ));
  }
}
